#OPC UA 16/09/2021 Industrial Robotics

from opcua import Server
from random import randint
import datetime
import time

server = Server()

url = "opc.tcp://192.168.1.199:4840" #port = application, chose yourself
server.set_endpoint(url)

name = "Industrial assignment"
addspace = server.register_namespace(name)

node = server.get_objects_node()

param = node.add_object(addspace, "Parameters")

temp = param.add_variable(addspace, "Temperature", 0) #name, init value
press = param.add_variable(addspace, "Pressure", 0)
Time = param.add_variable(addspace, "Time", 0)

temp.set_writable() #can overwrite
press.set_writable()
Time.set_writable()

server.start()
print("Starting @ {}".format(url))

while True:
        randomTemperature = randint(10,50)
        randomPressure = randint(200,999)
        currentTime = datetime.datetime.now()

        print(randomTemperature, randomPressure, currentTime)

        temp.set_value(randomTemperature) #overwrite prev. val
        press.set_value(randomPressure)
        Time.set_value(currentTime)

        time.sleep(1)