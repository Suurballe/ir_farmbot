from opcua import Client
import time

url = "opc.tcp://192.168.1.199:4840" #RPi addr
client = Client(url)
client.connect()
print("Connected...")

while True:
    temp = client.get_node("ns=2;i=2")
    temperature = temp.get_value()
    print(temperature)

    press = client.get_node("ns=2;i=3")
    pressure = press.get_value()
    print(pressure)

    Time = client.get_node("ns=2;i=4")
    Time = Time.get_value()
    print(Time)

    time.sleep(1)