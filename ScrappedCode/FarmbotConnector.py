import serial.tools.list_ports
import serial
import time

# Figure out what is plugged in

class FarmbotConnector:
    def __init__(self):
        self.baud_rate = 115200
        self.device_port = None


    def get_port(self):
        port_devices = serial.tools.list_ports.comports()

        for port, device, hwid in port_devices:
            # print(port, device, hwid)
            # INSERT CORRECT ID BELOW
            if hwid[12:21] == "2341:0042":
                self.device_port = port
        if self.device_port is None:
            raise Exception("No farmbot device has been found, check cable and power")

    def connect_to_farmbot(self):
        self.get_port()
        return serial.Serial(self.device_port, self.baud_rate, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)



if __name__ == '__main__':
    farmbotFinder = FarmbotConnector()
    farmbot = farmbotFinder.connect_to_farmbot()
    time.sleep(3)
    farmbot.write(b'OwO')
    while True:
        data = farmbot.readline()
        #data = farmbot.readline()[:-2]  # the last bit gets rid of the new-line chars
        if data:
            print(data)
