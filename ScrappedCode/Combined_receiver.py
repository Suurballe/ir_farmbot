import paho.mqtt.client as mqtt # import client
from opcua import Client #import client
import time

#IMPORTANT
#all 'client' variable names are for MQTT. All OPCUA functions will have OPCUA as a prefix.

#MQTT setup functions
def on_connect(client, userdata, flags, rc):
    print("Connected with the result code "+str(rc)+" - we good fam")
    client.subscribe("B2test")

def on_message(client, userdata, msg):
    print(msg.payload)

#MQTT setup connections & variables
MQTT_client = mqtt.Client()
MQTT_client.on_connect = on_connect
MQTT_client.on_message = on_message
MQTT_client.connect("mqtt.eclipseprojects.io")

#OPCUA setup
OPCUA_url = "opc.tcp://192.168.1.199:4840" #address of server (this is Hult's RPi)
OPCUA_client = Client(OPCUA_url)
OPCUA_client.connect()

while True:
    MQTT_client.loop_start() #loop automaticalls sends messages received - loop_forever() for neverending loop
    print("testing")

    #OPCUA readings
    temp = OPCUA_client.get_node("ns=2;i=2")
    temperature = temp.get_value()
    print(temperature)
    
    time.sleep(0.3)
    MQTT_client.loop_stop()