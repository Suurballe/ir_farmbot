import subprocess
import os
import stat
from pathlib import Path


class ServiceFileHandler:

    def __init__(self):
        self.service_name = "farmbotOPC.service"
        self.service_to_use = ["[Unit]",
                               "Description=OwO wats dis",
                               "",
                               "[Service]",
                               "ExecStart=/usr/bin/python3 /opt/test.py",
                               "Restart=on-failure",
                               "[Install]",
                               "WantedBy=multi-user.target",
                               ""]

    def create_service_file(self):
        # Prep contents first
        fixed_service_to_use = []
        for command in self.service_to_use:
            fixed_service_to_use.append(command)
            fixed_service_to_use.append("\n")

        # Create file, fill it, closes automatically
        with open(f'{self.service_name}', 'a') as service_file:
            for command in fixed_service_to_use:
                service_file.write(command)

    def move_file(self):
        subprocess.run(f'mv /opt/{self.service_name} /etc/systemd/system/')

    def load_and_setup_service(self):
        subprocess.run('systemctl daemon-reload')
        subprocess.run(f'systemctl enable {self.service_name}')
        subprocess.run(f'systemctl start {self.service_name}')

    def already_installed(self):
        file = Path(f"/etc/systemd/system/{self.service_name}")
        if file.is_file():
            return True
        else:
            return False


class Checks:

    @staticmethod
    def correct_working_directory():
        if os.getcwd() is '/opt':
            return True
        else:
            return False

    @staticmethod
    def process_launched_as_sudo():
        if os.geteuid() == 0:
            return True
        else:
            return False

class OtherInstallStuff:
    def __init__(self):
        self.permission_levels = "750"

    def set_permission_levels(self):
        try:
            subprocess.run(f'chmod {self.permission_levels} -R /opt')
        except:
            pass
