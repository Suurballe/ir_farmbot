import time
import paho.mqtt.client as mqtt   #importing the library itself, as a client

address="mqtt.eclipseprojects.io"   #public broker - use for test: mqtt.eclipseprojects.io
client = mqtt.Client("P1")   #creates new instance
#client.username_pw_set("TeamB2","mcp3202")
client.connect(address)   #connect
topic = "B2test"    #our topic subbed

while True:
    client.loop_start() #weird loop start
    print ("printing to client/sub")    #check if running
    client.publish(topic,"Hey! V-sauce.")   #publish random shit
    time.sleep(0.5)
    client.publish(topic,"Michael here.")
    time.sleep(0.5)
    client.publish(topic,"Where...")
    time.sleep(0.5)
    client.publish(topic,"Are..")
    time.sleep(0.5)
    client.publish(topic,"Your...")
    time.sleep(0.5)
    client.publish(topic,"Fingers?")
    time.sleep(2)
    client.loop_stop() #close weird loop