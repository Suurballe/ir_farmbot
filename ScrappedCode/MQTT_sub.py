import paho.mqtt.client as mqtt # import mqtt client

def on_connect(client, userdata, flags, rc):
    print("Connected with the result code "+str(rc))
    client.subscribe("B2test")

def on_message(client, userdata, msg):
    print(msg.payload)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("mqtt.eclipseprojects.io")

client.loop_forever() #keeps looking - needed to run