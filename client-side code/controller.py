import OPCUA_client
import farmbotConnector
from time import sleep


class Controller:
    def __init__(self):
        self.connector = farmbotConnector.FarmbotConnector()
        self.opc_client = OPCUA_client.OPCClient("192.168.1.199", "4840", "tcp")

        self.device = None
        self.last_command = None


    def check_for_new_data(self):
        temp_data = self.opc_client.get_data()
        if temp_data == self.last_command:
            return [False, ""]
        else:
            return [True, temp_data]

    def start(self):

        # Open connection to OPC
        while self.opc_client.connect() is False:
            sleep(1)

        while True:
            temp_data = self.check_for_new_data()
            if temp_data[0]:
                self.connector.write_to_farmbot(temp_data[1])
            sleep(0.5)

