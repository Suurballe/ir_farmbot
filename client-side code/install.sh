#!/bin/bash

if [ $HOSTNAME = raspberrypi ]; then
  mkdir /opt/farmbot_replacer/
  chmod 754 -R /opt/farmbot_replacer/
  cd /opt/farmbot/replacer/ || exit
  git clone https://gitlab.com/Suurballe/ir_farmbot.git
  pip3 install opcua
  mv ir_farmbot/client-side\ code/farmbotOPC.service /etc/systemd/system/
  systemctl daemon-reload
  systemctl enable farmbotOPC.service
fi

