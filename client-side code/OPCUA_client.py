from opcua import Client


class OPCClient():
    def __init__(self, ip_address, port, protocol):
        self.ip_address = ip_address
        self.port = port
        self.protocol = protocol
        self.client = None

    def connect(self):
        try:
            connection_string = f"opc.{self.protocol}://{self.ip_address}:{self.port}"
            self.client = Client(connection_string)
            self.client.connect()
            return True
        except:
            return False


    def get_data(self):
        if self.client is None:
            raise Exception("client is not connected. Did you forget to call '.connect()'?")
        data_from_server = self.client.get_node("ns=2;i=2")
        return data_from_server.get_value()
