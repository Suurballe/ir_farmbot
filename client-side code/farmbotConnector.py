import serial.tools.list_ports
import serial
from time import sleep


# Figure out what is plugged in

class FarmbotConnector:
    def __init__(self):
        self.baud_rate = 115200
        self.device_port = None
        #self.device = None

    def get_port(self):
        port_devices = serial.tools.list_ports.comports()

        for port, device, hwid in port_devices:
            # print(port, device, hwid)
            # INSERT CORRECT ID BELOW
            if hwid[12:21] == "2341:0042":
                self.device_port = port
        if self.device_port is None:
            raise Exception("No farmbot device has been found, check cable and power")

    def write_to_farmbot(self, command):
        if self.device_port is None:
            self.get_port()

        # Things to note: lower sleep, timeout changed to 0
        with serial.Serial(self.device_port, self.baud_rate, parity=serial.PARITY_NONE,
                                    stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=0) as ser:
            sleep(1)
            ser.write(str.encode("F22 P2 V1 Q0\r\n"))
            command += "\r\n"
            ser.write(str.encode(command))
