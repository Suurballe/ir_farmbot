import subprocess
import os
import stat
from pathlib import Path


class FileHandler:

    def __init__(self, service_name):
        self.service_name = service_name


    def move_file(self):
        subprocess.run(f'mv /opt/{self.service_name} /etc/systemd/system/', shell=True)

    def load_and_setup_service(self):
        subprocess.run('systemctl daemon-reload', shell=True)
        subprocess.run(f'systemctl enable {self.service_name}', shell=True)
        subprocess.run(f'systemctl start {self.service_name}', shell=True)

    @staticmethod
    def set_permission_levels(permission_level):
        try:
            subprocess.run(f'chmod {permission_level} -R /opt', shell=True)
        except:
            pass


class Checks(FileHandler):
    def __init__(self, service_name):
        super().__init__(service_name)
    
    def already_installed(self):
        file = Path(f"/etc/systemd/system/{self.service_name}")
        if file.is_file():
            return True
        else:
            return False

    @staticmethod
    def correct_working_directory():
        if os.getcwd() is '/opt':
            return True
        else:
            return False

    @staticmethod
    def process_launched_as_sudo():
        if os.geteuid() == 0:
            return True
        else:
            return False




'''
self.service_to_use = ["[Unit]",
                               "Description=OwO wats dis",
                               "",
                               "[Service]",
                               "ExecStart=/usr/bin/python3 /opt/test.py",
                               "Restart=on-failure",
                               "[Install]",
                               "WantedBy=multi-user.target",
                               ""]

    def create_service_file(self):
        # Prep contents first
        fixed_service_to_use = []
        for command in self.service_to_use:
            fixed_service_to_use.append(command)
            fixed_service_to_use.append("\n")

        # Create file, fill it, closes automatically
        with open(f'{self.service_name}', 'a') as service_file:
            for command in fixed_service_to_use:
                service_file.write(command)
'''