# IR_Farmbot

Industrial Application FarmBot robot project

## Desciption and commentary
The FarmBot is a robotic setup used for automating the process of growing crops. The FarmBot is currently intended for hobby use and by extension lacks the ability to communicate/interface with common industrial solutions.  
The FarmBot contains an RPi and a custom Arduino board. The Arduino board is used as a controller for the hardware placed on the FarmBot. This includes motor control and sensor readings. The RPi is hooked into the MQTT system and handles the communication between the MQTT network and the Arduino. The RPI and Arduino communicates using a serial connection and G + F codes.  
The current solution employed on the FarmBot relies heavily on MQTT and an offsite broker. This is usually undesirable for an industrial solution, as MQTT is not commonly used in industrial solutions and having full control of the network traffic is a requirement.  
Instead of MQTT, a solution using OPC UA is desired. Since the Arduino already handles the control of the hardware and the flexibility of the RPi, the desired solution involves using the RPi to host an UCP UA server that exposes the functionality of the Arduino.  

## Links
Offsite:

https://farm.bot/  
https://farm.bot/pages/open-source  
https://developer.farm.bot/v14/Documentation/farmbot-software-development  
https://developer.farm.bot/v14/Documentation/farmbot-software-development/high-level-overview  
https://opcfoundation.org/about/opc-technologies/opc-ua/  
https://en.wikipedia.org/wiki/OPC_Unified_Architecture  
https://www.youtube.com/watch?v=-tDGzwsBokY&ab_channel=TheOPCFoundation  
https://pypi.org/project/opcua/  

https://github.com/FarmBot/farmbot-arduino-firmware  

https://github.com/FarmBot/farmbot_os/blob/799b2a3a2c83bc195f629a2f4f9212a1de9e8caa/priv/farmware/measure-soil-height/serial_device.py

https://github.com/FarmBot/farmbot_os/blob/799b2a3a2c83bc195f629a2f4f9212a1de9e8caa/priv/farmware/measure-soil-height/settings.py#L78
