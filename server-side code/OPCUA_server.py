from opcua import Server


class CommandServer:
    def __init__(self, namespace, ip_address, port, protocol):
        self.namespace = namespace
        self.ip_address = ip_address
        self.port = port
        self.protocol = protocol

        self.server = Server()
        self.connection_string = f"opc.{self.protocol}://{self.ip_address}:{self.port}"
        self.server.set_endpoint(self.connection_string)
        self.server.register_namespace(self.namespace)
        self.node = self.server.get_objects_node()
        self.parameters = self.node.add_object(self.namespace, "commands")
        self.command_to_query = self.parameters.add_variable(namespace, "command", "")
        self.command_to_query.set_writeable()

        self.command_bank = None
        self.user_command = ""

    def load_command_file(self):
        with open("command_bank.txt") as file:
            self.command_bank = file.read().splitlines()
        if len(self.command_bank) < 1:
            Exception("Command file is empty")

    def start_server(self):
        self.server.start()

    def get_user_command(self):
        print("Choose a command to set for clients to pull and execute")
        command_number = 1
        for command in self.command_bank:
            print(f"{command_number}\t {command}")
            command_number += 1
        print("\n\n")
        user_choice = input("command: ")
        while user_choice != range(1, command_number, 1):
            user_choice = input("Invalid choice, try again: ")
        self.user_command = self.command_bank[int(user_choice) - 1]

    def set_user_command(self):
        self.command_to_query = self.user_command


class Controller:

    def __init__(self):
        self.server = CommandServer("Something", "192.168.1.199", "4840", "tcp")

    def start(self):
        self.server.load_command_file()
        self.server.start_server()
        while True:
            self.server.get_user_command()
            self.server.set_user_command()
